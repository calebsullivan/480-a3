all: 
	@javac Server.class ServerInterface.java Client.class

run:
	@echo "Usage: make client || make server"

server:
	@javac Server.java ServerInterface.java && java -cp ./ Server

client:
	@javac Client.java && java -cp ./ Client

clean:
	@rm *.class