import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
 
public class Server implements ServerInterface {
 
    public static final String REGSTR = "RMI_Demo";

    public Server() throws RemoteException {}
 
    public String getMessage() throws RemoteException {
        return "Hello world";
    }
 
    public static void main(String args[]) throws Exception {

        System.out.println("SERVER: RMI server has started with registry '" + REGSTR + "'");
        
        Server obj = new Server();
        try {
            ServerInterface stub = (ServerInterface) UnicastRemoteObject.exportObject(obj,0);
            Registry reg;
            try {
            	reg = LocateRegistry.createRegistry(1099);
                System.out.println("SERVER: java RMI registry created.");

            } catch(Exception e) {
            	reg = LocateRegistry.getRegistry();
                System.out.println("SERVER: registry exists");
            }
        	reg.rebind(REGSTR, stub);
        } catch (RemoteException e) {
            System.out.println("SERVER: " + e.getCause().getMessage());
        	e.printStackTrace();
            System.exit(0);
        }
    }
}
