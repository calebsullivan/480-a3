import java.rmi.ConnectException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Client { 

    public static final String REGSTR = "RMI_Demo";
    public static final String HOSTNAME = "localhost";

    public static void main(String args[]) throws Exception {
        try {
        	System.out.println("CLIENT: RMI client connecting to '" + HOSTNAME + "'");
        	Registry registry = LocateRegistry.getRegistry(HOSTNAME);
	        try { 
        		System.out.println("CLIENT: RMI client looking up '" + REGSTR + "'");
				ServerInterface obj = (ServerInterface) registry.lookup(REGSTR);
		    	System.out.println("CLIENT: " + obj.getMessage()); 
	        } catch (ConnectException e) {
	        	System.out.println("CLIENT: " + e.getCause().getMessage());
	        	System.exit(0);
	        }
        } catch (Exception e) {
            System.out.println("Client: " + e.getCause().getMessage());
        	e.printStackTrace();
            System.exit(0);
        }
    }
}